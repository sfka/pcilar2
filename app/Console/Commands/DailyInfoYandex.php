<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;


class DailyInfoYandex extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DailyInfoYandex:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update dashboard info from yandex';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = DB::table('users')->where([['yandexLogin', '<>', '']])->orderBy('created_at', 'asc')->get()->all();
        foreach ($users as $user) {
            app('App\Http\Controllers\DailyInfoByClient')->getYandexInfo($user->id);
        }
    }
}
