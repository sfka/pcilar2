<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\DailyInfoYandex',
        'App\Console\Commands\DailyInfoGoogle',
        'App\Console\Commands\DailyInfoFacebook',
        'App\Console\Commands\DailyInfoCallTrack',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('DailyInfoYandex:get')->dailyAt('01:00');
        $schedule->command('DailyInfoGoogle:get')->dailyAt('01:10');
        $schedule->command('DailyInfoFacebook:get')->dailyAt('01:20');
        $schedule->command('DailyInfoCallTrack:get')->dailyAt('01:30');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
