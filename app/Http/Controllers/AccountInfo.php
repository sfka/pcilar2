<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AccountInfo extends Controller
{
    public static function getClientInfoDB($dateFrom = NULL, $dateTo = NULL, $userID, $platform)
    {
        $arResult = [
            'GENERAL' => [
                'clicks' => 0,
                'impressions' => 0,
                'cost' => 0,
                'cost_per_click' => 0,
                'ctr' => 0,
                'balance' => 0,
                'forecast' => 0,
            ],
        ];
        $login = $platform . 'Login';

        if ($userID == NULL) {
            $userID = Auth::user()->id;
            $platformLogin = Auth::user()->$login;
        } else {
            $user = DB::table('users')->where('id', '=', $userID);
            $platformLogin = $user->$login;
        }
        if ($dateFrom == NULL) {
            $date['DateFrom'] = date('Y-m-d', strtotime('-7 days'));
        } else {
            $date['DateFrom'] = $dateFrom;
        }
        if ($dateTo == NULL) {
            $date['DateTo'] = date('Y-m-d', strtotime('-1 days'));
        } else {
            $date['DateTo'] = $dateTo;
        }

        $rows = DB::table($platform . '_info')->where('user_id', '=', $userID)
            ->whereBetween('date', [$date['DateFrom'], $date['DateTo']])
            ->orderBy('date', 'asc')->get()->all();

        $arResult['DAILY'] = $rows;

        foreach ($rows as $row) {
            //TODO: общие данные
            $arResult['GENERAL']['clicks'] += $row->clicks;
            $arResult['GENERAL']['impressions'] += $row->impressions;
            $arResult['GENERAL']['cost'] += $row->cost;
        }
        $arResult['GENERAL']['cost_per_click'] = round($arResult['GENERAL']['cost'] / $arResult['GENERAL']['clicks'], 2);
        $arResult['GENERAL']['ctr'] = round($arResult['GENERAL']['clicks'] / $arResult['GENERAL']['impressions'] * 100, 2);

        switch ($platform) {
            case 'yandex':
                $arResult['GENERAL']['balance'] = Yandex::getBalance($platformLogin);
                break;

            case 'google':
                $budget = AdWordsApiController::getSpendingLimit($platformLogin);
                $arResult['GENERAL']['balance'] = $budget - $arResult['GENERAL']['cost'];
                break;

            case 'facebook':
                $arResult['GENERAL']['balance'] = Facebook::getBalance($platformLogin);
                break;
        }

        $arResult['GENERAL']['forecast'] = round($arResult['GENERAL']['balance'] / ($arResult['GENERAL']['cost'] / count($arResult['DAILY'])));

        return $arResult;
    }

    public static function getClientInfoCallTrackDb($dateFrom = NULL, $dateTo = NULL, $userID, $platform)
    {
        $login = $platform . 'Login';
        if ($userID == NULL) {
            $userID = Auth::user()->id;
            $platformLogin = Auth::user()->$login;
        } else {
            $user = DB::table('users')->where('id', '=', $userID);
            $platformLogin = $user->$login;
        }
        if ($dateFrom == NULL) {
            $date['DateFrom'] = date('Y-m-d', strtotime('-7 days'));
        } else {
            $date['DateFrom'] = $dateFrom;
        }
        if ($dateTo == NULL) {
            $date['DateTo'] = date('Y-m-d', strtotime('-1 days'));
        } else {
            $date['DateTo'] = $dateTo;
        }
        $rows = DB::table($platform . '_info')->where('user_id', '=', $userID)
            ->whereBetween('date', [$date['DateFrom'], $date['DateTo']])
            ->orderBy('date', 'asc')->get()->all();
        $arResult['GENERAL']['answered'] = 0;
        $arResult['GENERAL']['no_answer'] = 0;
        $arResult['GENERAL']['busy'] = 0;
        $arResult['GENERAL']['failed'] = 0;
        foreach ($rows as $row) {
            $row->phones = unserialize($row->phones);
            $arResult['DAILY'][] = (array)$row;
            $arResult['GENERAL']['CALLERS'] = count($row->phones);
            foreach ($row->phones as $phone) {
                $arResult['GENERAL']['answered'] += (isset($phone['answered']) ? (int)$phone['answered'] : 0);
                $arResult['GENERAL']['no_answer'] += (isset($phone['no_answer']) ? (int)$phone['no_answer'] : 0);
                $arResult['GENERAL']['busy'] += (isset($phone['busy']) ? (int)$phone['busy'] : 0);
                $arResult['GENERAL']['failed'] += (isset($phone['failed']) ? (int)$phone['failed'] : 0);
            }
        }

        return $arResult;
    }
}
