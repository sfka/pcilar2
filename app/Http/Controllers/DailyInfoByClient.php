<?php

namespace App\Http\Controllers;

use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\AdWords\Reporting\v201809\DownloadFormat;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDownloader;
use Google\AdsApi\Common\OAuth2TokenBuilder;
use Google\AdsApi\AdWords\Query\v201809\ReportQueryBuilder;
use Google\AdsApi\AdWords\ReportSettingsBuilder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;

class DailyInfoByClient extends Controller
{
    private $yandex = [
        'token' => 'AgAAAAA5y5Z7AAZd0GIa4S3a60Jlk9JS1hSsp5o',
        'directUrl' => 'https://api.direct.yandex.com/json/v5/',
        'nds' => 20 / 100,
        'table' => 'yandex_info',
    ];

    private $google = [
        'table' => 'google_info',
    ];

    private $facebook = [
        'url' => 'https://graph.facebook.com/',
        'client_id' => '1451284475261549',
        'client_secret' => '3145bd2410f4ceee8efa5235e03154c4',
        'vApi' => 'v10.0',
        'shortLivedToken' => 'EAAUn73ZBdsm0BAIRp7vdnqLtCql9eDy289HlAzttgkvanGSop4ZCjQcPRuSosTZA1bLCsFmc5ZA5DBWXXuzHOwnT2Nah3IURJvTc7VqqEmrWN9BQGZACcjVkQoeVmvL5CneYgsEHZBeDgtU1x9AT503wGXVBZBUqZBpNCE7ZAHH9BF3Noxz5EGqA17vOu2SWQ1lDu1ZAkgX8RXTwZDZD',
        'longLivedToken' => 'EAAUn73ZBdsm0BAEp8iZAp20ytGtLdT3wlcQvCmZA6xIuxOpdomrkkjW00ZA1nsQSl3huPZAGwWze0UgOKDDCvLy034eQEuTsdhp35WpZCZCXB0i80Aud3uFOjKPheMgDut5rCslAeHQue6M7j3enoJmNkuuZB7ZBOuw1Y41JDSQQU9AZDZD',
        'table' => 'facebook_info',
    ];

    private $callTrack = [
        'table' => 'calltrack_info',
    ];

    private function getUsers($field)
    {
        $arResult = DB::table('users')
            ->where([[$field, '<>', '']])
            ->orderBy('created_at', 'asc')->get()->all();
        return $arResult;
    }

    private function getNextUser($user_id, $field)
    {
        $exists = DB::table('users')
            ->where('id', '>', $user_id)
            ->where([[$field, '<>', '']])
            ->orderBy('created_at', 'asc')->exists();
        if ($exists) {
            $user = DB::table('users')
                ->where('id', '>', $user_id)
                ->where([[$field, '<>', '']])
                ->orderBy('created_at', 'asc')
                ->get()->first();
            return $user;
        } else {
            return false;
        }
    }

    private function getDate($format)
    {
        $date['DateFrom'] = date($format, strtotime('-1 days'));
        $date['DateTo'] = date($format, strtotime('-1 days'));
        //$date['DateTo'] = date($format, strtotime('-1 days'));

        return $date;
    }

    public function getYandexInfo($user_id = '')
    {
        $users = $this->getUsers('yandexLogin');
        if ($user_id == '') {
            $arResult['USER_SELECTED'] = 'N';
            $arResult['USER_NEXT'] = current($users);
            return view('get_yandex')->with([
                'arResult' => $arResult,
            ]);
        } else {
            $arResult['USER_SELECTED'] = $user_id;
            $arResult['USER_NEXT'] = $this->getNextUser($user_id, 'yandexLogin');
            $user = DB::table('users')->where('id', $user_id)->first();

            $date = $this->getDate('Y-m-d');
            $yandex = $this->yandex;
            $params = [
                "params" => [
                    "SelectionCriteria" => $date,
                    "FieldNames" => [
                        "Clicks", "Cost", "Impressions",
                    ],
                    "ReportName" => "Отчет за все время",
                    "ReportType" => 'ACCOUNT_PERFORMANCE_REPORT',
                    "DateRangeType" => "CUSTOM_DATE",
                    "Format" => "TSV",
                    "IncludeVAT" => "NO",
                    "IncludeDiscount" => "NO",
                ],
            ];

            $headers = [
                "Authorization" => "Bearer {$yandex["token"]}",
                "Accept-Language" => "ru",
                "Content-Type" => "application/json; charset=utf-8",
                "skipReportHeader" => "true",
                "skipReportSummary" => "true",
            ];

            $headers = array_merge($headers, [
                "Client-Login" => "{$user->yandexLogin}",
            ]);
            $client = new Client([
                'base_uri' => $yandex['directUrl'],
                'headers' => $headers,
            ]);

            $res = $client->request('POST', 'reports', [
                'json' => $params,
            ])->getBody()->getContents();

            $res = $this->tsvToArray('N', 'Y', $res)[0];

            if (isset($res['Cost'])) {
                $res['Cost'] = round($res['Cost'] / 1000000, 2);

                $res['cost_per_click'] = round($res['Cost'] / $res['Clicks'], 2);
                $res['ctr'] = round($res['Clicks'] / $res['Impressions'] * 100, 2);
                $res['user_id'] = $user->id;
                $row['date_convert'] = date("d.m.y", strtotime('-1 days'));
                $res['date'] = date('Y-m-d', strtotime('-1 days'));

                $res = array_change_key_case($res, CASE_LOWER);

                DB::table($yandex['table'])->updateOrInsert(['user_id' => $user_id, 'date' => $res['date']], $res);
            }

            return view('get_yandex')->with([
                'arResult' => $arResult,
            ]);
        }
    }

    public function tsvToArray($first = 'N', $last = 'N', $textTSV)
    {
        $data = explode("\n", $textTSV);
        if ($first == 'Y') array_shift($data);
        if ($last == 'Y') array_pop($data);
        $keys = explode("\t", array_shift($data));
        $resultReport = array_map(function ($v) use ($keys) {
            if (isset($v) && count(explode("\t", $v)) == count($keys)) return array_combine($keys, explode("\t", $v));
            else return false;
        }, $data);

        return $resultReport;
    }

    public function getGoogleInfo($user_id = '')
    {
        $users = $this->getUsers('googleLogin');
        if ($user_id == '') {
            $arResult['USER_SELECTED'] = 'N';
            $arResult['USER_NEXT'] = current($users);
            return view('get_google')->with([
                'arResult' => $arResult,
            ]);
        } else {
            $arResult['USER_SELECTED'] = $user_id;
            $arResult['USER_NEXT'] = $this->getNextUser($user_id, 'googleLogin');
            $user = DB::table('users')->where('id', $user_id)->first();

            $date = $this->getDate('Ymd');
            $googleLogin = $user->googleLogin;
            $google = $this->google;

            $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile(config('app.adsapi_php_path'))->build();
            $session = (new AdWordsSessionBuilder())
                ->fromFile(config('app.adsapi_php_path'))
                ->withOAuth2Credential($oAuth2Credential)
                ->withClientCustomerId($googleLogin)
                ->build();
            $reportType = 'ACCOUNT_PERFORMANCE_REPORT';
            $reportRange = $date;
            $selectedFields = [
                "Clicks", "Cost", "Impressions",
            ];
            $collection = self::downloadReport(
                $reportType,
                $reportRange,
                new ReportDownloader($session),
                $selectedFields
            );

            $res = $collection[0]['@attributes'];

            if (isset($res['cost'])) {
                $res['cost'] = round($res['cost'] / 1000000, 2);

                $res['cost_per_click'] = round($res['cost'] / $res['clicks'], 2);
                $res['ctr'] = round($res['clicks'] / $res['impressions'] * 100, 2);
                $res['user_id'] = $user->id;
                $res['date_convert'] = date("d.m.y", strtotime('-1 days'));
                $res['date'] = date('Y-m-d', strtotime('-1 days'));

                $res = array_change_key_case($res, CASE_LOWER);

                DB::table($google['table'])->updateOrInsert(['user_id' => $user_id, 'date' => $res['date']], $res);
            }

            return view('get_google')->with([
                'arResult' => $arResult,
            ]);
        }
    }

    private function downloadReport(
        $reportType,
        $reportRange,
        ReportDownloader $reportDownloader,
        array $selectedFields
    )
    {
        $query = (new ReportQueryBuilder())
            ->select($selectedFields)
            ->from($reportType)
            ->during($reportRange['DateFrom'], $reportRange['DateTo'])->build();

        // For brevity, this sample app always excludes zero-impression rows.
        $reportSettingsOverride = (new ReportSettingsBuilder())
            ->includeZeroImpressions(false)
            ->build();
        $reportDownloadResult = $reportDownloader->downloadReportWithAwql(
            "$query",
            DownloadFormat::XML,
            $reportSettingsOverride
        );

        $json = json_encode(
            simplexml_load_string($reportDownloadResult->getAsString())
        );
        $resultTable = json_decode($json, true)['table'];

        if (array_key_exists('row', $resultTable)) {
            // When there is only one row, PHP decodes it by automatically
            // removing the containing array. We need to add it back, so the
            // "view" can render this data properly.
            $row = $resultTable['row'];
            $row = count($row) > 1 ? $row : [$row];
            return collect($row);
        }

        // No results returned for this query.
        return collect([]);
    }

    public function getFacebookInfo($user_id = '')
    {
        $users = $this->getUsers('facebookLogin');
        if ($user_id == '') {
            $arResult['USER_SELECTED'] = 'N';
            $arResult['USER_NEXT'] = current($users);
            return view('get_facebook')->with([
                'arResult' => $arResult,
            ]);
        } else {
            $arResult['USER_SELECTED'] = $user_id;
            $arResult['USER_NEXT'] = $this->getNextUser($user_id, 'facebookLogin');
            $user = DB::table('users')->where('id', $user_id)->first();

            $date = $this->getDate('Y-m-d');
            $facebookLogin = $user->facebookLogin;
            $facebook = $this->facebook;

            $fieldNames = [
                "clicks", "spend", "impressions",
            ];
            $fieldNamesBalance = ['balance'];

            $time_range = 'time_range={"since":"' . $date['DateFrom'] . '","until":"' . $date['DateTo'] . '"}';
            $url = '/act_' . $facebookLogin . '/insights?fields=' . implode(',', $fieldNames) . '&' . $time_range . '&access_token=' . $facebook['longLivedToken'];

            $client = new Client([
                //'base_uri' => $facebook['url'] . $facebook['vApi'],
            ]);

            $res = json_decode($client->request('GET', $facebook['url'] . $facebook['vApi'] . $url)->getBody()->getContents());
            $res = $res->data;

            if (isset($res['cost'])) {
                $res['cost'] = $res['spend'];

                $res['cost_per_click'] = round($res['spend'] / $res['clicks'], 2);
                $res['ctr'] = round($res['clicks'] / $res['impressions'] * 100, 2);
                $res['user_id'] = $user->id;
                $res['date_convert'] = date("d.m.y", strtotime('-1 days'));
                $res['date'] = date('Y-m-d', strtotime('-1 days'));

                $res = array_change_key_case($res, CASE_LOWER);

                DB::table($facebook['table'])->updateOrInsert(['user_id' => $user_id, 'date' => $res['date']], $res);
            }

            return view('get_facebook')->with([
                'arResult' => $arResult,
            ]);
        }
    }

    public function getCallTrackInfo($user_id = '')
    {
        $users = $this->getUsers('callTrackLogin');
        if ($user_id == '') {
            $arResult['USER_SELECTED'] = 'N';
            $arResult['USER_NEXT'] = current($users);
            return view('get_calltrack')->with([
                'arResult' => $arResult,
            ]);
        } else {
            $arResult['USER_SELECTED'] = $user_id;
            $arResult['USER_NEXT'] = $this->getNextUser($user_id, 'callTrackLogin');
            $user = DB::table('users')->where('id', $user_id)->first();

            $date = $this->getDate('Y-m-d');
            $callTrackLogin = $user->callTrackLogin;
            $callTrack = $this->callTrack;
            $token = $this->getCallTrackToken($user);

            $dateRange = 'start-date=' . $date['DateFrom'] . '&end-date=' . $date['DateTo'];

            $client = new Client([
                'base_uri' => 'https://calltracking.ru/',
            ]);
            $callTrackProject = $user->callTrackProject;
            $url = 'api/get_data.php?project=' . $callTrackProject . '&auth=' . $token . '&dimensions=ct:date,ct:caller,ct:status&metrics=ct:calls&sort=-ct:datetime&max-results=2000&start-index=0&' . $dateRange;
            $res = json_decode($client->request('GET', $url)->getBody()->getContents());

            if (isset($res->data)) {
                $arResult = json_decode(json_encode($res->data), TRUE);
            }
            foreach ($arResult as $keyCt => $ctField) {
                $arResult[$keyCt]['TOTAL_A'] = 0;
                $arResult[$keyCt]['TOTAL_N'] = 0;
                $arResult[$keyCt]['TOTAL_B'] = 0;
                $arResult[$keyCt]['TOTAL_F'] = 0;
                foreach ($ctField as $key => $field) {
                    if (isset($field['ANSWERED'])) $arResult[$keyCt]['TOTAL_A'] += $field['ANSWERED']['ct:calls'];
                    if (isset($field['NO ANSWER'])) $arResult[$keyCt]['TOTAL_N'] += $field['NO ANSWER']['ct:calls'];
                    if (isset($field['BUSY'])) $arResult[$keyCt]['TOTAL_B'] += $field['BUSY']['ct:calls'];
                    if (isset($field['FAILED'])) $arResult[$keyCt]['TOTAL_F'] += $field['FAILED']['ct:calls'];

                    $row['phones'][] = [
                        'number' => $key,
                        'answered' => (isset($field['ANSWERED']['ct:calls']) ? $field['ANSWERED']['ct:calls'] : ''),
                        'no_answer' => (isset($field['NO ANSWER']['ct:calls']) ? $field['NO ANSWER']['ct:calls'] : ''),
                        'busy' => (isset($field['BUSY']['ct:calls']) ? $field['BUSY']['ct:calls'] : ''),
                        'failed' => (isset($field['FAILED']['ct:calls']) ? $field['FAILED']['ct:calls'] : ''),
                    ];
                }
                $row['total_answered'] = $arResult[$keyCt]['TOTAL_A'];
                $row['total_no_answered'] = $arResult[$keyCt]['TOTAL_N'];
                $row['total_busy'] = $arResult[$keyCt]['TOTAL_B'];
                $row['total_failed'] = $arResult[$keyCt]['TOTAL_F'];
                $row['user_id'] = $user->id;
                $row['date_convert'] = date("d.m.y", strtotime('-1 days'));
                $row['date'] = $keyCt;
            }
            $row['phones'] = serialize($row['phones']);

            DB::table($callTrack['table'])->updateOrInsert(['user_id' => $user_id, 'date' => $row['date']], $row);

            return view('get_calltrack')->with([
                'arResult' => $arResult,
            ]);
        }
    }

    public function getCallTrackToken($user)
    {
        $client = new Client([
            'base_uri' => 'https://calltracking.ru/',
        ]);

        $callTrackLogin = $user->callTrackLogin;
        $callTrackPass = $user->callTrackPassword;

        $url = 'api/login.php?account_type=calltracking&login=' . $callTrackLogin . '&password=' . $callTrackPass . '&service=analytics';

        $res = json_decode($client->request('GET', $url)->getBody()->getContents());

        return $res->data;
    }
}
