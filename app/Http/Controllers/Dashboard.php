<?php

namespace App\Http\Controllers;

use App\Http\Controllers\CallTracking;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Dashboard extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    private $platforms = [
        'Yandex', 'Facebook', 'Google', 'CallTrack',
    ];

    public function index(Request $request)
    {
        $fields = $request->all();

        $dateRange = [
            'DateFrom' => (isset($fields['dateFrom1']) ? date('d.m.Y', strtotime($fields['dateFrom1'])) : date('d.m.Y', strtotime('-7 days'))),
            'DateTo' => (isset($fields['dateTo1']) ? date('d.m.Y', strtotime($fields['dateTo1'])) : date('d.m.Y', strtotime('-1 days'))),
        ];
        $dateRangeToSession = [
            'DateFrom' => (isset($fields['dateFrom1']) ? $fields['dateFrom1'] : date('Y-m-d', strtotime('-7 days'))),
            'DateTo' => (isset($fields['dateTo1']) ? $fields['dateTo1'] : date('Y-m-d', strtotime('-1 days'))),
        ];
        $arResult['dateRange'] = $dateRange;

        if (Auth::user()->hasRole('user')) {
            if (isset($fields['platform'])) {
                $arResult['PLATFORM'] = $fields['platform'];
                $arResult['DATA'] = self::getArrayDashboard($fields['platform'], $dateRangeToSession);
            } else {
                foreach ($this->platforms as $platform) {
                    $arResult['PLATFORM'] = $platform;
                    $arResult['DATA'] = self::getArrayDashboard($platform, $dateRangeToSession);
                    break;
                }
            }
        } elseif (Auth::user()->hasRole('admin') || Auth::user()->hasRole('manager')) {
            $arResult['USERS'] = Role::where('name', 'user')->first()->users()->get();
            if (isset($fields['user_name'])) {
                $arResult['PLATFORM'] = $fields['platform'];
                $arResult['DATA'] = self::getArrayDashboard($fields['platform'], $dateRangeToSession, NULL, $fields['user_name']);
            }
        }
        return view('dashboard')->with([
            'arResult' => $arResult,
        ]);
    }

    public function getCompare(Request $request)
    {
        $fields = $request->all();
        if (isset($fields['dateFrom1']) && isset($fields['dateTo1']) && isset($fields['dateFrom2']) && isset($fields['dateTo2'])) {
            $defaultDate1 = [
                'DateFrom' => $fields['dateFrom1'],
                'DateTo' => $fields['dateTo1'],
            ];
            $defaultDate2 = [
                'DateFrom' => $fields['dateFrom2'],
                'DateTo' => $fields['dateTo2'],
            ];
        } else {
            $defaultDate1 = [
                'DateFrom' => date('Y-m-d', strtotime('-7 days')),
                'DateTo' => date('Y-m-d', strtotime('-1 days')),
            ];
            $defaultDate2 = [
                'DateFrom' => date('Y-m-d', strtotime('-14 days')),
                'DateTo' => date('Y-m-d', strtotime('-8 days')),
            ];
        }

        if (isset($fields['platform'])) {
            $arResult = self::getArrayDashboard($fields['platform'], $defaultDate1, $defaultDate2);
        }
        return view('contents.compare.compare-list')->with([
            'arResult' => $arResult,
        ]);
    }

    /**
     * @param $platform
     * @param $date1
     * @param null $date2
     * @param null $userID
     * @return mixed
     */
    public function getArrayDashboard($platform, $date1, $date2 = NULL, $userID = NULL)
    {
        if ($platform != 'CallTrack') {
            $clientInfo1 = AccountInfo::getClientInfoDB($date1['DateFrom'], $date1['DateTo'], $userID, strtolower($platform));
            if ($date2 != NULL) {
                $clientInfo2 = AccountInfo::getClientInfoDB($date2['DateFrom'], $date2['DateTo'], $userID, strtolower($platform));
            }
        } else {
            $clientInfo1 = AccountInfo::getClientInfoCallTrackDb($date1['DateFrom'], $date1['DateTo'], $userID, strtolower($platform));
            if ($date2 != NULL) {
                $clientInfo2 = AccountInfo::getClientInfoDB($date2['DateFrom'], $date2['DateTo'], $userID, strtolower($platform));
            }
        }
        $arResult['INFO']['1'] = $clientInfo1;
        $arResult['Date']['1'] = $date1;
        if ($date2 != NULL) {
            $arResult['Date']['2'] = $date2;
            $arResult['INFO']['2'] = $clientInfo2;
        }

        $arResult['Date']['1']['DateFrom'] = date('d.m.Y', strtotime($date1['DateFrom']));
        $arResult['Date']['1']['DateTo'] = date('d.m.Y', strtotime($date1['DateTo']));
        if ($date2 != NULL) {
            $arResult['Date']['2']['DateFrom'] = date('d.m.Y', strtotime($date2['DateFrom']));
            $arResult['Date']['2']['DateTo'] = date('d.m.Y', strtotime($date2['DateTo']));
        }
        return $arResult;
    }

    public static function getCompareNumber($num1, $num2)
    {
        $text = '';
        if ($num1 < $num2) {
            $text .= '<span><span class="minus-triangle triangle"></span>';
            $perc = round(100 - round($num1 / $num2 * 100, 1), 1);
            $text .= $perc . '%</span>';
        } else {
            $text .= '<span><span class="plus-triangle triangle"></span>';
            $perc = round(round($num1 / $num2 * 100, 1) - 100, 1);
            $text .= $perc . '%</span>';
        }
        return $text;
    }
}
