<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Mail;

class Leads extends Controller
{
    private $table = 'leads_user';
    private $venyooFields = ['Телефон', 'Дата', 'utm_source', 'utm_medium', 'utm_term'];
    private $siteFields = ['Телефон', 'Date', 'Источник', 'Канал', 'Ключевое слово'];
    private $calltrackFields = ['Входящий номер', 'Дата', 'Источник', 'Канал', 'Запрос'];

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('manager')) {
            $arResult['USERS'] = Role::where('name', 'user')->first()->users()->get();
        } else {
            $arResult['USER'] = Auth::user()->id;
        }
        if ($request->isMethod('post')) {
            $fields = $request->all();
            if (!isset($fields['dateFrom'])) {
                $date['DateFrom'] = date('Y-m-d', strtotime('-7 days'));
            } else {
                $date['DateFrom'] = $fields['dateFrom'];
            }
            if (!isset($fields['dateTo'])) {
                $date['DateTo'] = date('Y-m-d', strtotime('-1 days'));
            } else {
                $date['DateTo'] = $fields['dateTo'];
            }
            if (isset($fields['user_name'])) {
                $arResult['LEADS'] = self::getLeads($date, $fields['user_name']);
            } else {
                $arResult['LEADS'] = self::getLeads($date, Auth::user()->id);
            }
            $arResult['Date'] = $date;
        }
        return view('leads.index')->with([
            'arResult' => $arResult,
        ]);
    }

    public function loadLeads(Request $request)
    {
        if ($request->isMethod('post')) {
            $file = new Filesystem;;
            $file->cleanDirectory(public_path() . '/tmp_files/');

            $fields = $request->all();
            $arResult = [];
            if ($request->hasFile('venyoo_file')) {
                $fileVenyoo = $request->file('venyoo_file');
                $arResultVenyoo = self::getArrayByFile($fileVenyoo, $this->venyooFields, 'Чат');
                $arResult = array_merge_recursive($arResult, $arResultVenyoo);
            }
            if ($request->hasFile('site_file')) {
                $fileSite = $request->file('site_file');
                $arResultSite = self::getArrayByFile($fileSite, $this->siteFields, 'Сайт');
                $arResult = array_merge_recursive($arResult, $arResultSite);
            }
            if ($request->hasFile('calltrack_file')) {
                $fileCt = $request->file('calltrack_file');
                $arResultCt = self::getArrayByFile($fileCt, $this->calltrackFields, 'Звонок');
                $arResult = array_merge_recursive($arResult, $arResultCt);
            }
            $date = date('Y-m-d H:i:s');


            usort($arResult, function ($a1, $a2) {
                if ($a1[1] == $a2[1]) return 0;

                return (strtotime($a1[1]) < strtotime($a2[1])) ? -1 : 1;
            });
            $rows = [];
            foreach ($arResult as $row) {
                $rows[$row[0]] = $row;
            }
            unset($arResult);
            $arResult = $rows;
            $countRow = 0;
            foreach ($arResult as $key => $row) {
                $dateRow = date('Y-m-d H:i:s', strtotime($row[1]));
                $arResult[$key][1] = $dateRow;
                $row[0] = '+' . substr($row[0], 0, 1) . ' (' . substr($row[0], 1, 3) . ') ' . substr($row[0], 4, 3) . '-' . substr($row[0], 7, 2) . '-' . substr($row[0], 9, 2);
                $dbRow = DB::table($this->table)->where([['phone', '=', $row[0]], ['date', '=', $dateRow]])->get()->first();
                if (isset($dbRow->phone)) {
                    continue;
                } else {
                    $countRow++;
                    DB::table($this->table)->insert(['user_id' => $fields['user_name'], 'phone' => $row[0], 'date' => date('Y-m-d H:i:s', strtotime($row[1])), 'source' => $row[2], 'channel' => $row[3], 'word' => $row[4], 'file' => $row[5], 'created_at' => $date]);
                    $arResult['NEW_LEADS'][$countRow]['phone'] = $row[0];
                    $arResult['NEW_LEADS'][$countRow]['date'] = date('d.m.Y H:i:s', strtotime($row[1]));
                    $arResult['NEW_LEADS'][$countRow]['source'] = $row[2];
                    $arResult['NEW_LEADS'][$countRow]['channel'] = $row[3];
                    $arResult['NEW_LEADS'][$countRow]['word'] = $row[4];
                    $arResult['NEW_LEADS'][$countRow]['file'] = $row[5];
                }
                unset($dbRow);
            }
            // TODO: отправка почты (необходжимо указаать в config/mail.php данные
            /*Mail::raw('Привет', function($message)
            {
                $message->from(env('MAIL_USERNAME', 'asd'), 'Vasya Pupkin');
                $message->to('ildarfatez@gmail.com');
            }); */
        }


        if (isset($countRow) && $countRow > 0) $arResult['success_message'] = 'Лидов добавлено в базу: ' . $countRow;
        else $arResult['error'] = 'Новых лидов не добавлены';
        return view('leads.load')->with([
            'arResult' => $arResult,
        ]);
        //return redirect()->route('leads', ['id' => 1]);
    }

    public function getArrayByFile($file, $fields, $source)
    {
        $file->move(public_path() . '/tmp_files', $file->getClientOriginalName());
        $file_n = (public_path() . '/tmp_files/' . $file->getClientOriginalName());
        $file = fopen($file_n, "r");
        $keys = [];
        $countRow = 0;
        while (($data = fgetcsv($file, 20000, ";")) !== FALSE) {
            $countRow++;
            $countKeys = 0;
            foreach ($data as $keyField => $field) {
                foreach ($fields as $keyVenyoo => $venyooField) {
                    if ($field == $venyooField) {
                        $keys[$keyVenyoo] = $keyField;
                        $countKeys++;
                    }
                }
                ksort($keys);
            }
            if ($countKeys == count($fields)) {
                continue;
            } else {
                foreach ($keys as $i => $key) {
                    $arResult[$countRow][] = $data[$key];
                }
                $arResult[$countRow][] = $source;
            }
        }
        fclose($file);
        foreach ($arResult as $key => $row) {
            if (count($row) < count($fields)) {
                unset($arResult[$key]);
            } else {
                $phone = $row[0];
                $phone = preg_replace('~\D+~', '', $phone);
                $phone = ltrim($phone, '+');
                if (strpos($phone, '8') == 0 && mb_strlen($phone) == 11 && $phone != '') {
                    $phone[0] = '7';
                }
                if (mb_strlen($phone) < 11 && $phone != '') {
                    $phone = '7' . $phone;
                }
                $arResult[$key][0] = $phone;
                //$arResult[$key][1] = strtotime($row[1]);
            }
        }
        return $arResult;
    }

    public function getLeads($date, $user = 'all')
    {
        if (isset($date['DateTo'])) {
            $dateFile['DateTo'] = $date['DateTo'];
            $date['DateTo'] = date('Y-m-d', strtotime($date['DateTo'] . ' + 1 day'));
        }
        if ($user != 'all') {
            $dbRow = DB::table($this->table)->where([['user_id', '=', $user]])->whereBetween('date', [$date['DateFrom'], $date['DateTo']])->orderBy('date', 'desc')->get()->all();
        } else {
            $dbRow = DB::table($this->table)->whereBetween('date', [$date['DateFrom'], $date['DateTo']])->get()->all();
        }
        foreach ($dbRow as $row) {
            $leadsID[] = $row->id;
        }
        $userName = DB::table('users')->where('id', $user)->get()->first();
        $fileName = $userName->name . '_' . date('d.m.Y', strtotime($date['DateFrom'])) . '_' . date('d.m.Y', strtotime($dateFile['DateTo'])) . '_leads.csv';
        if (isset($leadsID) && count($leadsID) > 0) {
            self::downloadLeads($leadsID, $fileName);
        }
        $dbRow['LEADS_USER'] = $dbRow;
        $dbRow['FILE'] = $fileName;
        return $dbRow;
    }

    public function updateLead(Request $request)
    {
        $fields = $request->all();
        if (isset($fields['id']) && isset($fields['checked'])) {
            DB::table($this->table)->where([['id', '=', $fields['id']]])->update(['valid' => $fields['checked']]);
        }
        if (isset($fields['id']) && isset($fields['comment'])) {
            DB::table($this->table)->where([['id', '=', $fields['id']]])->update(['comment' => $fields['comment']]);
        }
        return;
    }

    public function downloadLeads($arLeads, $fileName)
    {
        if (!empty($arLeads)) {

            header('Content-Type: text/csv; charset=utf-8');
            header('Content-Disposition: attachment; filename=' . $fileName);
            $output = fopen(public_path() . '/tmp_files/' . $fileName, 'w');
            $arResult['FILE_PATH'] = public_path() . '/tmp_files/' . $fileName;
            fputcsv($output, ['Телефон', 'Дата', 'Источник', 'Канал', 'Запрос', 'Файл', 'Валид', 'Комментарий'], ';');


            foreach ($arLeads as $leadID) {
                $dbLead = (array)DB::table($this->table)->where('id', $leadID)->get()->first();
                fputcsv($output, [$dbLead['phone'], $dbLead['date'], $dbLead['source'], $dbLead['channel'], $dbLead['word'], $dbLead['file'], $dbLead['valid'], $dbLead['comment']], ';');
            }
            fclose($output);
            foreach ($arLeads as $leadID) {
                $dbLead = (array)DB::table($this->table)->where('id', $leadID)->get()->first();
            }
        }
        $arResult['success'] = 'asdasd';
        return $arResult;
    }
}
