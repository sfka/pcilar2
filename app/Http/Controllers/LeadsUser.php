<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class LeadsUser extends Controller
{
    protected $table = 'venyoo_leads';
    protected $venyooCol = 'widget_id';
    protected $tildaCol = 'tranid';
    protected $callCol = 'project_id';

    public function index(Request $request)
    {
        if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('manager')) {
            $arResult['USERS'] = Role::where('name', 'user')->first()->users()->get();
        } else {
            $arResult['USER'] = Auth::user()->id;
        }
        if ($request->isMethod('post')) {
            $fields = $request->all();
            if (!isset($fields['dateFrom'])) {
                $date['DateFrom'] = date('Y-m-d', strtotime('-7 days'));
            } else {
                $date['DateFrom'] = $fields['dateFrom'];
            }
            if (!isset($fields['dateTo'])) {
                $date['DateTo'] = date('Y-m-d', strtotime('-1 days'));
            } else {
                $date['DateTo'] = $fields['dateTo'];
            }
            if (isset($fields['user_name'])) {
                $arResult['LEADS'] = self::getLeadsByUser($date, $fields['user_name']);
            } else {
                $arResult['LEADS'] = self::getLeadsByUser($date, Auth::user()->id);
            }
            $arResult['Date'] = $date;
        }
        /*echo'<pre>';
        print_r($arResult);
        echo'</pre>';*/
        return view('leads.index')->with([
            'arResult' => $arResult,
        ]);
    }

    public function getLeadsByUser($date, $user = 'all')
    {
        $arResult = [];
        if (isset($date['DateTo'])) {
            $dateFile['DateTo'] = $date['DateTo'];
            $date['DateTo'] = date('Y-m-d', strtotime($date['DateTo'] . ' + 1 day'));
        }
        if ($user != 'all') {
            $userInfo = DB::table('users')->where('id', $user)->first();
            $venyooID = $userInfo->venyoo_project;
            $tildaID = $userInfo->tilda_project;
            $callID = $userInfo->callTrackProject;
            $dbRow = DB::table($this->table)->where([[$this->venyooCol, '=', $venyooID]])
                ->orWhere([[$this->tildaCol, 'LIKE', $tildaID . '%']])
                ->orWhere([[$this->callCol, 'LIKE', $callID . '%']])
                ->whereBetween('created_at', [$date['DateFrom'], $date['DateTo']])->orderBy('created_at', 'asc')->get()->all();
        } else {
            $dbRow = DB::table($this->table)->whereBetween('created_at', [$date['DateFrom'], $date['DateTo']])->orderBy('created_at', 'asc')->get()->all();
        }
        foreach ($dbRow as $row) {
            if (isset($row->tranid)) {
                $row->file = 'Tilda';
                $row->lead_page = explode('?', $row->roistat_url)[0];
            }
            if (isset($row->widget_id)) {
                $row->file = 'Venyoo';
                $row->lead_page = explode('?', $row->ref_host)[0];
            }
            if (isset($row->project_id)) {
                $row->phone = $row->caller;
                $row->file = 'CallTrack';
                $row->lead_page = explode('?', $row->landing)[0];
            }
            $leadsID[] = $row->id;
            $arResult[$row->phone] = $row;
        }
        $arResult = array_reverse($arResult);
        return $arResult;
    }

    public function updateLead(Request $request)
    {
        $fields = $request->all();
        if (isset($fields['id']) && isset($fields['checked'])) {
            DB::table($this->table)->where([['id', '=', $fields['id']]])->update(['valid' => $fields['checked']]);
        }
        if (isset($fields['id']) && isset($fields['comment'])) {
            DB::table($this->table)->where([['id', '=', $fields['id']]])->update(['comment' => $fields['comment']]);
        }
        return;
    }

    public function getLeadsFromWebhook(Request $request)
    {
        $setRow = true;
        $dbColumns = Schema::getColumnListing($this->table);
        $insertRow = [];
        foreach ($request->all() as $key => $reqCol) {
            if (strtolower($key) == 'id') continue;
            if (strtolower($key) == 'phone' || strtolower($key) == 'caller') {
                $reqCol = preg_replace('~\D+~', '', $reqCol);
                $reqCol = '+7' . preg_replace(
                        '/^(\d)(\d{3})(\d{3})(\d{2})(\d{2})$/',
                        '(\2)\3-\4-\5',
                        (string)$reqCol
                    );
                if (strlen($reqCol) < 16) $setRow = false;
            }
            if (!in_array(strtolower($key), $dbColumns)) {
                Schema::table($this->table, function ($table) use ($key) {
                    $table->string(strtolower($key))->nullable();
                });
            }
            $insertRow[$key] = $reqCol;
        }
        if ($setRow) {
            DB::table($this->table)->insert([$insertRow]);
        }

        echo "ok";
    }
}
