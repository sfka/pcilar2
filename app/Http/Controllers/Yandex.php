<?php

namespace App\Http\Controllers;

use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Yandex extends Controller
{
    public static $token = 'AgAAAAA5y5Z7AAZd0GIa4S3a60Jlk9JS1hSsp5o';
    public static $directUrl = 'https://api.direct.yandex.com/json/v5/';
    public static $nds = 20 / 100;
    public static $metricGoals = [
        'Site (Lead)',
        'Chat (Lead)',
        'Call (Lead)',
        'Reserve (KPI)',
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        //$campaigns = self::getCampaignsList();
        return view('yandex')->with([
            //'campaigns' => $campaigns,
        ]);
    }

    private static $REPORT_TYPE_TO_DEFAULT_SELECTED_FIELDS = [
        'CAMPAIGN_PERFORMANCE_REPORT' => [
            "Date", "CampaignName", "LocationOfPresenceName", "Impressions", "Clicks", "Cost",
            "AdFormat", "AdNetworkType", "Age", "AvgClickPosition", "AvgCpc", "AvgImpressionPosition", "AvgPageviews", "BounceRate", "Bounces", "CampaignType", "CarrierType", "ConversionRate", "Conversions", "CostPerConversion", "Ctr", "Device", "ExternalNetworkName", "Gender", "GoalsRoi", "LocationOfPresenceId", "MatchType", "MobilePlatform", "Placement", "Profit", "Revenue", "Sessions", "Slot", "TargetingLocationId", "TargetingLocationName",
        ],
        'ADGROUP_PERFORMANCE_REPORT' => [
            "AdGroupName", "AvgImpressionPosition", "AvgClickPosition", "BounceRate", "Bounces", "CampaignName", "Clicks", "Impressions", "Revenue", "Slot", "TargetingLocationName",
        ],
        'AD_PERFORMANCE_REPORT' => [
            "AdId",
        ],
        'ACCOUNT_PERFORMANCE_REPORT' => [
            "Clicks", "Cost", "Conversions", "ConversionRate", "Ctr", "Impressions", "AvgCpc", "CostPerConversion",
        ],
    ];

    public function getCampaignsList(Request $request)
    {
        $fieldNames = ['Id', 'Name'];
        if (isset($request->fields)) {
            foreach ($request->fields as $field) {
                $fieldNamesNew[] = $field;
            }
        } else {
            $fieldNamesNew = [];
        }
        $fieldNames = array_merge($fieldNames, $fieldNamesNew);
        if ($request->input('entriesPerPage')) {
            $pageLimit = $request->input('entriesPerPage');
        }
        if ($request->input('pageOffset')) {
            $pageOffset = $request->input('pageOffset');
        } else {
            $pageOffset = 0;
        }
        $params = [
            'method' => 'get',
            'params' => [
                'SelectionCriteria' => (object)[
                    'States' => ['ON'],
                ],
                'FieldNames' => $fieldNames,
                "Page" => [
                    "Limit" => $pageLimit,
                    "Offset" => $pageOffset,
                ],
            ],
        ];
        $curlRequest = self::getCurlRequest(self::$directUrl . 'campaigns', $params);


        $result = ($curlRequest['result'] ? $curlRequest['result'] : '');
        $responseHeaders = ($curlRequest['responseHeaders'] ? $curlRequest['responseHeaders'] : '');
        $responseBody = ($curlRequest['responseBody'] ? $curlRequest['responseBody'] : '');
        $httpCode = ($curlRequest['httpCode'] ? $curlRequest['httpCode'] : '');

        $resp = (array)json_decode($responseBody);
        if (isset($resp['error']->error_string)) {
            $campaigns['error'] = $resp['error']->error_string;
            $paging = [];
        }

        if (isset($resp['result'])) {
            $limit = (array)json_decode($responseBody)->result;
            if (isset($limit['LimitedBy'])) {
                $limit = $limit['LimitedBy'];
            } else {
                $limit = 0;
            }

            if ($limit >= $pageOffset) {
                $paging = [
                    "Limit" => 1,
                    "Offset" => $limit,
                    "Next" => $limit + $pageLimit,
                    "Prev" => $pageOffset - $pageLimit,
                    "fields" => $request->fields,
                ];
            } else {
                $paging = [
                    "Limit" => 1,
                    "Offset" => $request->input('pageOffset'),
                    "Prev" => $pageOffset - $pageLimit,
                    "Next" => "N",
                    "fields" => $request->fields,
                ];
            }
            $campaigns = $resp['result']->Campaigns;
            foreach ($campaigns as $key => $campaign) {
                $campaigns[$key] = (array)$campaign;
            }
        }
        return view('contents.yandex.campaigns')->with([
            'campaigns' => $campaigns,
            'selectedFields' => $fieldNames,
            'paging' => $paging,
        ]);
    }

    public function getReport(Request $request)
    {
        $userName = Auth::user()->id;
        if ($request->input('dateFrom') && $request->input('dateTo')) {
            $date['DateFrom'] = $request->input('dateFrom');
            $date['DateTo'] = $request->input('dateTo');
        } else {
            $date['DateFrom'] = date('Y-m-d', strtotime('-7 days'));
            $date['DateTo'] = date('Y-m-d', strtotime('-1 days'));
        }
        $fieldNames = [];
        if (isset($request->fields)) {
            foreach ($request->fields as $field) {
                $fieldNamesNew[] = $field;
            }
        } else {
            $fieldNamesNew = [];
        }
        $fieldNames = array_merge($fieldNames, $fieldNamesNew);
        $reportName = [
            'CAMPAIGN_PERFORMANCE_REPORT' => "Отчет №" . strtotime($date['DateTo']) . " по кампании за период",
            'ADGROUP_PERFORMANCE_REPORT' => "Отчет по рекламной группе за период",
            'AD_PERFORMANCE_REPORT' => "Отчет по рекламе за период",
            'ACCOUNT_PERFORMANCE_REPORT' => "Отчет по аккаунту за период",
        ];
        $params = [
            "params" => [
                "SelectionCriteria" => $date,
                "FieldNames" => $fieldNames,
                "ReportName" => $reportName[$request->input('reportType')] . $date['DateFrom'] . $date['DateTo'],
                "ReportType" => $request->input('reportType'),
                "DateRangeType" => "CUSTOM_DATE",
                "Format" => "TSV",
                "IncludeVAT" => "NO",
                "IncludeDiscount" => "NO",
            ],
        ];
        $fieldNames = $params['params']['FieldNames'];

        if (Session::get('yandexClientInfoReport' . $userName . $reportName[$request->input('reportType')] . 'Date_' . $date['DateFrom'] . '_' . $date['DateTo'] . 'Fields_' . implode('', $fieldNames))) {
            $resultReport = Session::get('yandexClientInfoReport' . $userName . $reportName[$request->input('reportType')] . 'Date_' . $date['DateFrom'] . '_' . $date['DateTo'] . 'Fields_' . implode('', $fieldNames));
        } else {
            $curlRequest = self::getCurlRequest(self::$directUrl . 'reports', $params, []);
            $result = ($curlRequest['result'] ? $curlRequest['result'] : '');
            $responseHeaders = ($curlRequest['responseHeaders'] ? $curlRequest['responseHeaders'] : '');
            $responseBody = ($curlRequest['responseBody'] ? $curlRequest['responseBody'] : '');
            $httpCode = ($curlRequest['httpCode'] ? $curlRequest['httpCode'] : '');
            $resultReport = self::tsvToArray('Y', 'Y', $responseBody);
            if (isset($resultReport[0]['Cost']) && is_numeric($resultReport[0]['Cost'])) $resultReport[0]['Cost'] = round($resultReport[0]['Cost'] / 1000000, 1);
            if (isset($resultReport[0]['AvgCpc']) && is_numeric($resultReport[0]['AvgCpc'])) $resultReport[0]['AvgCpc'] = round($resultReport[0]['AvgCpc'] / 1000000, 1);
            if (isset($resultReport[0]['CostPerConversion']) && is_numeric($resultReport[0]['CostPerConversion'])) $resultReport[0]['CostPerConversion'] = round($resultReport[0]['CostPerConversion'] / 1000000, 1);
            if (empty($resultReport[0]))
                $resultReport['error'] = 'Неизвестная ошибка при формировании отчета, попробуйте позже';
            Session::put('yandexClientInfoReport' . $userName . $reportName[$request->input('reportType')] . 'Date_' . $date['DateFrom'] . '_' . $date['DateTo'] . 'Fields_' . implode('', $fieldNames), $resultReport);
        }


        return view('contents.yandex.report-results')->with([
            'arResult' => $resultReport,
            'selectedFields' => $fieldNames,
        ]);
    }

    public function getClientInfo($dateFrom = NULL, $dateTo = NULL, $userID)
    {
        if ($userID == NULL) {
            $yandexLogin = Auth::user()->yandexLogin;
            $userName = Auth::user()->id;
            $yandexCounterId = Auth::user()->yandexCounterId;
        } else {
            $user = DB::table('users')->where('id', $userID)->first();
            $yandexLogin = $user->yandexLogin;
            $userName = $user->id;
            $yandexCounterId = $user->yandexCounterId;
        }
        if ($dateFrom == NULL) {
            $date['DateFrom'] = date('Y-m-d', strtotime('-7 days'));
        } else {
            $date['DateFrom'] = $dateFrom;
        }
        if ($dateTo == NULL) {
            $date['DateTo'] = date('Y-m-d', strtotime('-1 days'));
        } else {
            $date['DateTo'] = $dateTo;
        }
        if (isset($yandexLogin) && $yandexLogin != '') {
            if (Session::get('yandexClientInfo' . $userName . 'Date_' . $date['DateFrom'] . '_' . $date['DateTo'])) {
                $yandexClientInfo = Session::get('yandexClientInfo' . $userName . 'Date_' . $date['DateFrom'] . '_' . $date['DateTo']);
            } else {
                $params = [
                    'method' => 'get',
                    'params' => [
                        'SelectionCriteria' => (object)[
                            //'States' => ['ON']
                        ],
                        'FieldNames' => ['Id', 'Name', 'Funds', 'Currency', 'DailyBudget', 'Statistics', 'StartDate', 'EndDate'],
                    ],
                ];
                $yandexClientInfo = [
                    'Clicks' => 0,
                    'Impressions' => 0,
                    'Balance' => 0,
                    'Cost' => 0,
                    'AvgCpc' => 0,
                ];

                //из reports

                $paramsReport = [
                    "params" => [
                        "SelectionCriteria" => $date,
                        "FieldNames" => self::$REPORT_TYPE_TO_DEFAULT_SELECTED_FIELDS['ACCOUNT_PERFORMANCE_REPORT'],
                        "ReportName" => "Отчет за все время",
                        "ReportType" => 'ACCOUNT_PERFORMANCE_REPORT',
                        "DateRangeType" => "CUSTOM_DATE",
                        "Format" => "TSV",
                        "IncludeVAT" => "NO",
                        "IncludeDiscount" => "NO",
                    ],
                ];
                $newHeaders = [
                    "skipReportHeader: true",
                    "skipReportSummary: true",
                ];
                $curlRequest = self::getCurlRequest(self::$directUrl . 'reports', $paramsReport, $newHeaders, 'N', 'post', $yandexLogin);

                $responseBodyReport = ($curlRequest['responseBody'] ? $curlRequest['responseBody'] : '');

                if (isset($responseBodyReport)) {
                    $resultReport = self::tsvToArray('N', 'N', $responseBodyReport);
                    if (isset($resultReport[0]) && !empty($resultReport[0])) {
                        $yandexClientInfo = array_merge($yandexClientInfo, $resultReport[0]);
                    }
                }
                $arVisits = self::getVisits($date['DateFrom'], $date['DateTo'], $yandexCounterId);
                $yandexClientInfo = array_merge($yandexClientInfo, $arVisits);

                $yandexClientInfo['Balance'] = self::getBalance($yandexLogin);
                $yandexClientInfo['Cost'] = ($yandexClientInfo['Cost'] + $yandexClientInfo['Cost'] * self::$nds) / 1000000;
                $yandexClientInfo['AvgCpc'] = ($yandexClientInfo['AvgCpc'] + $yandexClientInfo['AvgCpc'] * self::$nds) / 1000000;
                if (isset($yandexClientInfo['metric']['totalGoalsVisits']) && $yandexClientInfo['metric']['totalGoalsVisits'] > 0) {
                    $yandexClientInfo['OrderCost'] = round($yandexClientInfo['Cost'] / $yandexClientInfo['metric']['totalGoalsVisits'], 0);
                }
                $dayDiff = (strtotime($dateTo) - strtotime($dateFrom)) / 86400 + 1;
                if ($yandexClientInfo['Cost'] > 0) {
                    $yandexClientInfo['forecast'] = floor($yandexClientInfo['Balance'] / ($yandexClientInfo['Cost'] / $dayDiff));
                } else {
                    $yandexClientInfo['forecast'] = 0;
                }
                if ($yandexClientInfo['Impressions'] > 0) {
                    $yandexClientInfo['Ctr'] = round(($yandexClientInfo['Clicks'] / $yandexClientInfo['Impressions']) * 100, 1);
                } else {
                    $yandexClientInfo['Ctr'] = 0;
                }
                if (isset($yandexClientInfo['goalConversion']['metric']['visits']) && isset($yandexClientInfo['goalConversion']['metric']['totalGoalsVisits'])) {
                    $yandexClientInfo['goalConversion'] = round($yandexClientInfo['goalConversion']['metric']['visits'] / $yandexClientInfo['goalConversion']['metric']['totalGoalsVisits'], 2);
                }
                Session::put('yandexClientInfo' . $userName . 'Date_' . $date['DateFrom'] . '_' . $date['DateTo'], $yandexClientInfo);
            }
        } else {
            $yandexClientInfo['error'] = 'У данного аккаунта не установлен yandexLogin';
        }

        return $yandexClientInfo;
    }

    public function getClientInfoPage()
    {
        $yandexClientInfo = $this->getClientInfo();
        return view('contents.yandex.client-info')->with([
            'arResult' => $yandexClientInfo,
        ]);
    }

    public function getVisits($dateFrom, $dateTo, $yandexCounterId = NULL)
    {
        $dimensions = [
            'ym:s:lastTrafficSource',
            'ym:s:lastSearchEngineRoot',
            'ym:s:lastSocialNetwork',
        ];
        $metrics = [
            'ym:s:visits',
            'ym:s:users',
        ];
        $goalIDs = self::getGoals($yandexCounterId);
        if (is_array($goalIDs)) {
            foreach ($goalIDs as $key => $goalID) {
                $goal_ID_visits[$key] = 'ym:s:goal' . $goalID['id'] . 'visits';
            }
            $goal_ID_visitsTxt = implode(',', $goal_ID_visits);
        } else {
            $goal_ID_reachesTxt = '';
            //$goal_ID_visitsTxt = '';
        }
        $dimensionsTxt = implode(',', $dimensions);
        $metricsTxt = implode(',', $metrics);

        if ($yandexCounterId == NULL) {
            $yandexCounterId = Auth::user()->yandexCounterId;
        }
        //$url = 'https://api-metrika.yandex.net/stat/v1/data?dimensions=' . $dimensionsTxt . '&metrics=' . $metricsTxt . ',' . $goal_ID_visitsTxt . '&id=' . $yandexCounterId . '&lang=ru&limit=1000';
        if (isset($goal_ID_visitsTxt)) {
            $url = 'https://api-metrika.yandex.net/stat/v1/data?dimensions=' . $dimensionsTxt . '&metrics=' . $metricsTxt . ',' . $goal_ID_visitsTxt . '&id=' . $yandexCounterId . '&lang=ru&limit=1000&date1=' . $dateFrom . '&date2=' . $dateTo;
        } else {
            $url = 'https://api-metrika.yandex.net/stat/v1/data?dimensions=' . $dimensionsTxt . '&metrics=' . $metricsTxt . '&id=' . $yandexCounterId . '&lang=ru&limit=1000&date1=' . $dateFrom . '&date2=' . $dateTo;
        }
        $headers = [
            "Authorization: OAuth " . self::$token,
        ];

        $arVisits = self::getCurlRequest($url, [], $headers, 'Y', 'get');
        $arVisits = json_decode($arVisits['responseBody']);
        $arResult = [];
        if (is_array($goalIDs)) {
            $arResult['goals'] = $goalIDs;
        }
        $totalGoalsVisits = 0;
        if (isset($arVisits->query->metrics)) {
            foreach ($arVisits->query->metrics as $key => $metric) {
                $metricName = str_replace('ym:s:', '', $metric);
                preg_match_all("/(?'goalID'[\d]+)(?'goalName'[\D]+)/", $metricName, $arMetricName, PREG_SET_ORDER);
                if (isset($arMetricName[0]['goalID'])) {
                    $arResult['goals'][$arMetricName[0]['goalID']][$arMetricName[0]['goalName']] = $arVisits->totals[$key];
                    $totalGoalsVisits += $arVisits->totals[$key];
                }
                if (empty($arMetricName[0]['goalID'])) {
                    $arResult['metric'][$metricName] = $arVisits->totals[$key];
                }
            }
        }

        $arResult['metric']['totalGoalsVisits'] = $totalGoalsVisits;

        return $arResult;
    }

    public static function getGoals($yandexCounterId = NULL)
    {
        if ($yandexCounterId == NULL) {
            $yandexCounterId = Auth::user()->yandexCounterId;
        }
        $url = 'https://api-metrika.yandex.net/management/v1/counter/' . $yandexCounterId . '/goals';
        $headers = [
            "Authorization: OAuth " . self::$token,
        ];

        $arGoals = self::getCurlRequest($url, [], $headers, 'Y', 'get');
        $arGoals = json_decode($arGoals['responseBody']);
        if (isset($arGoals->goals)) {
            foreach ($arGoals->goals as $goal) {
                if (in_array($goal->name, self::$metricGoals)) {
                    $goalIDs[$goal->id]['id'] = $goal->id;
                    $goalIDs[$goal->id]['name'] = $goal->name;
                }
            }
        }
        if (empty($goalIDs)) $goalIDs = 'Нет данных';
        return $goalIDs;
    }

    public static function getBalance($yandexLogin)
    {
        $url = 'https://api.direct.yandex.ru/v4/json/';
        $params = [
            'method' => "AccountManagement",
            'param' => [
                'Action' => "Get",
                'SelectionCriteria' => [
                    'Logins' => [$yandexLogin],
                ],
            ],
            'locale' => "ru",
            'token' => self::$token,
        ];
        $headers = [
            'POST /live/v4/json/ HTTP/1.1',
            'Host: api.direct.yandex.ru',
            'Authorization: Bearer ' . self::$token,
            'Accept-Language: ru',
            'Client-Login:  ' . $yandexLogin,
            'Content-Type: application/json; charset=utf-8',
        ];
        $url = 'https://api.direct.yandex.ru/live/v4/json/';


        $client = new Client([
            'headers' => $headers,
        ]);

        $res = $client->request('POST', $url, [
            'json' => $params,
        ])->getBody()->getContents();
        $arBalance = json_decode($res);

        if (isset($arBalance->data)) {
            $balance = $arBalance->data->Accounts[0]->Amount;
        } else {
            $balance = 0;
        }

        return $balance;
    }

    public function getClients()
    {
        $params = [
            'method' => 'get',
            'params' => [
                'FieldNames' => ["AccountQuality", "Archived", "ClientId", "ClientInfo", "CountryId", "CreatedAt", "Currency", "Grants", "Login", "Notification", "OverdraftSumAvailable", "Phone", "Representatives", "Restrictions", "Settings", "Type", "VatRate"],
            ],
        ];
        $url = 'https://api.direct.yandex.com/v5/agencyclients';
        $token = self::$token;
        $headers = [
            "Authorization: Bearer $token",
            "Accept-Language: ru",
            "Content-Type: application/json; charset=utf-8",
        ];
        $arClients = self::getCurlRequest($url, $params, $headers, 'Y', 'get');
        return $arClients;
    }

    public static function getCurlRequest($url, $params, $newHeaders = [], $changeHeader = 'N', $type = 'post', $yandexLogin = NULL)
    {
        if ($yandexLogin == NULL) {
            $yandexLogin = Auth::user()->yandexLogin;
        }

        $token = self::$token;
        $clientLogin = $yandexLogin;

        if ($changeHeader == 'Y') {
            $headers = $newHeaders;
        } else {
            $headers = [
                "Authorization: Bearer $token",
                "Client-Login: $clientLogin",
                "Accept-Language: ru",
                "Content-Type: application/json; charset=utf-8",
            ];
            $headers = array_merge($headers, $newHeaders);
        }
        $body = json_encode($params, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        if ($type != 'get') {
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
        }
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($curl);

        if (!$result) {
            $arResult['error'] = 'Ошибка cURL: ' . curl_errno($curl) . ' - ' . curl_error($curl);
        } else {
            $responseHeadersSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
            $responseHeaders = substr($result, 0, $responseHeadersSize);
            $responseBody = substr($result, $responseHeadersSize);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $arResult = [
                'result' => $result,
                'responseHeaders' => $responseHeaders,
                'responseBody' => $responseBody,
                'httpCode' => $httpCode,
            ];
        }
        curl_close($curl);

        return $arResult;
    }

    public function tsvToArray($first = 'N', $last = 'N', $textTSV)
    {
        $data = explode("\n", $textTSV);
        if ($first == 'Y') array_shift($data);
        if ($last == 'Y') array_pop($data);
        $keys = explode("\t", array_shift($data));
        $resultReport = array_map(function ($v) use ($keys) {
            if (isset($v) && count(explode("\t", $v)) == count($keys)) return array_combine($keys, explode("\t", $v));
            else return false;
        }, $data);

        return $resultReport;
    }
}
