@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col s12">
            <h4>Редактировать пользователя {{ $user->name }}</h4>
        </div>
    </div>
    <div class="row">
        <div class="col s12">
            <form action="{{ route('admin.users.update', $user) }}" method="POST">
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="name">Имя</label>
                        <input id="name" type="text" class="validate @error('name') invalid @enderror" name="name" value="{{ $user->name }}" required />
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="email">E-mail</label>
                        <input id="email" type="email" class="validate @error('email') invalid @enderror" name="email" value="{{ $user->email }}" required autocomplete="email">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="googleLogin">Google Login</label>
                        <input id="googleLogin" type="text" class="validate @error('googleLogin') invalid @enderror" name="googleLogin" value="{{ $user->googleLogin }}" />
                        @error('googleLogin')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="yandexLogin">Yandex Login</label>
                        <input id="yandexLogin" type="text" class="validate @error('yandexLogin') invalid @enderror" name="yandexLogin" value="{{ $user->yandexLogin }}" />

                        @error('yandexLogin')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="yandexCounterId">Yandex Counter</label>
                        <input id="yandexCounterId" type="text" class="validate @error('yandexCounterId') invalid @enderror" name="yandexCounterId" value="{{ $user->yandexCounterId }}">

                        @error('yandexCounterId')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="facebookLogin">Facebook Login</label>
                        <input id="facebookLogin" type="text" class="validate @error('facebookLogin') invalid @enderror" name="facebookLogin" value="{{ $user->facebookLogin }}">
                        @error('facebookLogin')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="callTrackLogin">CallTracking Login</label>
                        <input id="callTrackLogin" type="text" class="validate @error('callTrackLogin') invalid @enderror" name="callTrackLogin" value="{{ $user->callTrackLogin }}">

                        @error('callTrackLogin')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="callTrackPassword">CallTracking Password</label>
                        <input id="callTrackPassword" type="text" class="validate @error('callTrackPassword') invalid @enderror" name="callTrackPassword" value="{{ $user->callTrackPassword }}">
                        @error('callTrackPassword')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="callTrackProject">CallTracking Project</label>
                        <input id="callTrackProject" type="text" class="validate @error('callTrackProject') invalid @enderror" name="callTrackProject" value="{{ $user->callTrackProject }}">
                        @error('callTrackProject')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="venyooProject">Venyoo Project</label>
                        <input id="venyooProject" type="text" class="validate @error('venyooProject') invalid @enderror" name="venyoo_project" value="{{ $user->venyoo_project }}">
                        @error('venyooProject')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <label for="tildaProject">Tilda Project</label>
                        <input id="tildaProject" type="text" class="validate @error('tildaProject') invalid @enderror" name="tilda_project" value="{{ $user->tilda_project }}">
                        @error('tildaProject')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                @csrf
                <div class="row">
                    <div class="col s12 input-field">
                        {{ method_field('PUT') }}
                        <div class="checkbox-list">
                            @foreach($roles as $role)
                                <label>
                                    <input class="filled-in" type="checkbox" name="roles[]" value="{{ $role->id }}"
                                           @if($user->roles->pluck('id')->contains($role->id)) checked @endif>
                                    <span>{{ $role->name }}</span>
                                </label>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12 input-field">
                        <button type="submit" class="btn btn-large waves-effect waves-light">Обновить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
