@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <form class="col m6 s12 offset-s3" method="POST" action="{{ route('login') }}">
            @csrf
            <div class="row">
                <div class="col s12">
                    <h4>{{ __('Вход') }}</h4>
                </div>
            </div>
            <div class="row">
                <div class="col s12 input-field">
                    <label for="email">{{ __('E-mail') }}</label>
                    <input id="email" type="email" class="validate @error('email') invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="col s12 input-field">
                    <label for="password">{{ __('Пароль') }}</label>
                    <input id="password" type="password" class="validate @error('password') invalid @enderror" name="password" required autocomplete="current-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror

                    @if (Route::has('password.request'))
                        <a href="{{ route('password.request') }}">
                            {{ __('Забыли пароль?') }}
                        </a>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col s12 input-field">
                    <p>
                        <label>
                            <input type="checkbox" class="filled-in" {{ old('remember') ? 'checked' : '' }} />
                            <span>{{ __('Запомнить меня') }}</span>
                        </label>
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col s12 input-field">
                    <button class="waves-effect waves-light btn btn-large" type="submit">
                        {{ __('Войти') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
