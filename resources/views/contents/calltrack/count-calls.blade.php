@if (isset($countCalls))
    <? $ct = $countCalls; ?>
    <div class="col s12">
        <div class="dynamic-content__inner">
            <?$itogCt = 0; ?>
            <div class="table_wrap">
                <table class="table">
                    <thead>
                    <tr>
                        @foreach($ct as $keyCt => $ctField)
                            <th class="tac">{{ date('d.m.Y', strtotime($keyCt)) }}</th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        @foreach($ct as $keyCt => $ctField)
                            <td style="vertical-align: baseline">
                                @foreach($ctField as $key => $field)
                                    @if($key != 'TOTAL_A' && $key != 'TOTAL_N' && $key != 'TOTAL_B' && $key != 'TOTAL_F')
                                        <div class="table_wrap">
                                            <table class="table">
                                                <tr>
                                                    <td>{{ $key }}</td>
                                                    <td style="min-width: 150px">
                                                        @if(isset($field['ANSWERED']))
                                                            отвеченных: {{ $field['ANSWERED']['ct:calls'] }}
                                                        @endif
                                                        @if(isset($field['NO ANSWER']))
                                                            неотвеченных: {{ $field['NO ANSWER']['ct:calls'] }}
                                                        @endif
                                                        @if(isset($field['BUSY']))>
                                                        занято во время звонка: {{ $field['BUSY']['ct:calls'] }}
                                                        @endif
                                                        @if(isset($field['FAILED ']))
                                                            ошибка оборудования: {{ $field['FAILED']['ct:calls'] }}
                                                        @endif
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    @endif
                                @endforeach
                            </td>
                        @endforeach
                    </tr>
                    <tr>
                        @foreach($ct as $keyCt => $ctField)
                            <td>
                                @if(($ctField['TOTAL_A']) > 0)
                                    <p>отвеченных: {{ $ctField['TOTAL_A'] }}</p>
                                    <?$itogCt += $ctField['TOTAL_A'] ?>
                                @endif
                                @if(($ctField['TOTAL_N']) > 0)
                                    <p>неотвеченных: {{ $ctField['TOTAL_N'] }}</p>
                                    <?$itogCt += $ctField['TOTAL_N'] ?>
                                @endif
                                @if(($ctField['TOTAL_B']) > 0)
                                    <p>занято во время звонка: {{ $ctField['TOTAL_B'] }}</p>
                                    <?$itogCt += $ctField['TOTAL_B'] ?>
                                @endif
                                @if(($ctField['TOTAL_F']) > 0)
                                    <p>ошибка оборудования: {{ $ctField['TOTAL_F'] }}</p>
                                    <?$itogCt += $ctField['TOTAL_F'] ?>
                                @endif
                            </td>
                        @endforeach
                    </tr>
                    </tbody>
                </table>
            </div>
            <li class="collection-item">
                Общее количество за выбранный период: {{ $itogCt }}
            </li>
        </div>
    </div>
@endif
@if(isset($paging))
    @if($paging['Prev'] !== 'N')
        <form action="{{ url('calltrack/count-calls') }}" method="POST" class="ajax__form">
            {{ csrf_field() }}
            <input type="hidden" name="start" value="{{ $paging['Prev'] }}">
            <input type="hidden" name="entriesPerPage" value="{{ $paging['Limit'] }}">
            <input type="hidden" name="dateFrom" value="{{ $paging['dateFrom'] }}">
            <input type="hidden" name="dateTo" value="{{ $paging['dateTo'] }}">
            <button type="submit" class="btn btn-primary">Prev</button>
        </form>
    @endif
    @if($paging['Next'] > 0)
        <form action="{{ url('calltrack/count-calls') }}" method="POST" class="ajax__form">
            {{ csrf_field() }}
            <input type="hidden" name="start" value="{{ $paging['Next'] }}">
            <input type="hidden" name="entriesPerPage" value="{{ $paging['Limit'] }}">
            <input type="hidden" name="dateFrom" value="{{ $paging['dateFrom'] }}">
            <input type="hidden" name="dateTo" value="{{ $paging['dateTo'] }}">
            <button type="submit" class="btn btn-primary">Next</button>
        </form>
    @endif
@endif
