@if(isset($campaignInfo))
    <div class="container-fluid mt-2">
        @if (isset($campaignInfo['0']))
            <div class="table_wrap">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Var</th>
                        <th scope="col">Value</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($campaignInfo['0'] as $index => $campaign)
                        <tr>
                            <td>{{ $index }}</td>
                            <td>
                                @if(isset($campaign) && gettype($campaign) == 'string')
                                    {{ $campaign }}
                                @elseif(gettype($campaign) == 'array')
                                    @foreach($campaign as $key => $campField)
                                        <p>
                                            <span><b>{{ $campField->action_type }}: </b></span>
                                            <span>{{ $campField->value }}</span>
                                        </p>
                                    @endforeach
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @elseif(isset($campaignInfo['error']))
            <p>{{ $campaignInfo['error'] }}</p>
        @endif
    </div>
@endif
