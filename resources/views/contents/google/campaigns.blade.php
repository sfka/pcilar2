<div class="container-fluid mt-2">
    <div class="table_wrap">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                @foreach ($selectedFields as $field)
                    <th scope="col">{{ $field }}</th>
                @endforeach
            </tr>
            </thead>
            <tbody>
            @foreach ($campaigns as $index => $campaign)
                <tr>
                    <th scope="row">{{ $index + 1 + ($campaigns->currentPage() - 1) * $campaigns->perPage() }}</th>
                    @foreach ($selectedFields as $field)
                        <?php $methodName = 'get' . $field ?>
                        @if (empty($campaign->$methodName()))
                            <td><em>N/A</em></td>
                        @else
                            <td>{{ $campaign->$methodName() }}</td>
                        @endif
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

{{ $campaigns->links() }}
