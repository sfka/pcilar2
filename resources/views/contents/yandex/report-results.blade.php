@if(isset($arResult['error']))
    <div class="container-fluid mt-2">
        <p>{{ $arResult['error'] }}</p>
    </div>
@else
    <div class="container-fluid mt-2">
        <div class="table_wrap">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Fields</th>
                    @foreach ($arResult as $index => $res)
                        @if(is_array($res))
                            <th scope="col">{{ $index + 1 }}</th>
                        @endif
                    @endforeach
                </tr>
                </thead>
                <tbody>
                @foreach($selectedFields as $field)
                    <tr>
                        <td> {{ $field }}</td>
                        @foreach ($arResult as $index => $res)
                            @if(isset($res[$field]))
                                <td>{{ $res[$field] }}</td>
                            @endif
                        @endforeach
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endif
