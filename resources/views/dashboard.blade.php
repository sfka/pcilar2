@extends('layouts.app')
@section('content')
    <div class="dashboard" id="dashboard">
        <div class="row">
            <div class="col s12">
                <h3 class="dashboard__heading">Отчет по аккаунту за период {{ $arResult['dateRange']['DateFrom'] }}
                    - {{ $arResult['dateRange']['DateTo'] }}</h3>
            </div>
        </div>
        <div class="row">
            <div class="col s12">
                <form action="/" method="POST" class="ajax__form">
                    {{ csrf_field() }}
                    @if(isset($arResult['USERS']))
                        <div class="row">
                            <div class="col m6 s12 input-field">
                                <select name="user_name">
                                    @foreach($arResult['USERS'] as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                </select>
                                <label>Идентификатор клиента</label>
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col m6 s12 input-field">
                            <select name="platform">
                                @if(isset(Auth::user()->yandexLogin))
                                    <option>Yandex</option>@endif
                                @if(isset(Auth::user()->googleLogin))
                                    <option>Google</option>@endif
                                @if(isset(Auth::user()->facebookLogin))
                                    <option>Facebook</option>@endif
                                @if(isset(Auth::user()->callTrackLogin))
                                    <option>CallTrack</option>@endif
                            </select>
                            <label>Социальные сети:</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col m6 s12 input-field">
                            <input id="dateFrom1" class="datepicker" type="text" name="dateFrom1">
                            <label for="dateFrom1">Дата начала: dd / mm / yyyy</label>
                        </div>
                        <div class="col m6 s12 input-field">
                            <input id="dateTo1" class="datepicker" type="text" name="dateTo1">
                            <label for="dateTo1">Дата конца: dd / mm / yyyy</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col m6 s12 input-field">
                            <button class="btn waves-effect waves-light" type="submit">
                                Получить информацию
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="dynamic-content content_form">
            @if($arResult['PLATFORM'] != 'CallTrack')
                @if(isset($arResult['DATA']['INFO']['1']['GENERAL']))
                    <? $genData = $arResult['DATA']['INFO']['1']['GENERAL'];?>
                    <div class="row">
                        <div class="col s12">
                            <div class="dynamic-content__inner">
                                <h4>{{ $arResult['PLATFORM'] }}</h4>
                                <ul class="collection with-header">
                                    <li class="collection-header">
                                        <b>Бюджет</b>
                                    </li>
                                    @if(isset($genData['cost']))
                                        <li class="collection-item">
                                            Расход: {{ $genData['cost'] }} ₽
                                        </li>
                                    @endif
                                    @if(isset($genData['balance']))
                                        <li class="collection-item">
                                            Сумма остатка: {{ $genData['balance'] }} ₽
                                        </li>
                                    @endif
                                    @if(isset($genData['forecast']))
                                        <li class="collection-item">
                                            Прогноз бюджета (дней): {{ $genData['forecast'] }}
                                        </li>
                                    @endif
                                    <li class="collection-header">
                                        <b>Статистика</b>
                                    </li>
                                    @if(isset($genData['cost_per_click']))
                                        <li class="collection-item">
                                            Стоимость клика: {{ $genData['cost_per_click'] }} ₽
                                        </li>
                                    @endif
                                    @if(isset($genData['clicks']))
                                        <li class="collection-item">
                                            Количество кликов: {{ $genData['clicks'] }}
                                        </li>
                                    @endif
                                    @if(isset($genData['impressions']))
                                        <li class="collection-item">
                                            Количество показов: {{ $genData['impressions'] }}
                                        </li>
                                    @endif
                                    @if(isset($genData['ctr']))
                                        <li class="collection-item">
                                            Ctr: {{ $genData['ctr'] }} %
                                        </li>
                                    @endif
                                    <li class="collection-header">
                                        <b>Лиды</b>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif
            @else
                <? $ct = $arResult['DATA']['INFO']['1']['DAILY']; ?>
                <? $gen = $arResult['DATA']['INFO']['1']['GENERAL']; ?>
                <div class="col s12">
                    <div class="dynamic-content__inner">
                        <h4>{{ $arResult['PLATFORM'] }}</h4>
                        <?$itogCt = 0; ?>
                        <table>
                            <thead>
                            <tr>
                                @foreach($ct as $ctField)
                                    <th class="tac">{{ $ctField['date_convert'] }}</th>
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                @foreach($ct as $ctField)
                                    <td style="vertical-align: top">
                                        <table>
                                            <thead>
                                            <tr>
                                                <td>Номер</td>
                                                <td>Отвеченных</td>
                                                <td>Неотвеченных</td>
                                                <td>Занятых</td>
                                                <td>Ошибок</td>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($ctField['phones'] as $field)
                                                <tr>
                                                    <td>
                                                        {{ $field['number'] }}
                                                    </td>
                                                    @if(isset($field['answered']))
                                                        <td>{{ $field['answered'] }}</td>
                                                    @endif
                                                    @if(isset($field['no_answer']))
                                                        <td>{{ $field['no_answer'] }}</td>
                                                    @endif
                                                    @if(isset($field['busy']))
                                                        <td>{{ $field['busy'] }}</td>
                                                    @endif
                                                    @if(isset($field['failed']))
                                                        <td>{{ $field['failed'] }}</td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </td>
                                @endforeach
                            </tr>
                            </tbody>
                        </table>
                        <p class="collection-item">
                            Общее количество за выбранный период: {{ $itogCt }}
                        </p>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
