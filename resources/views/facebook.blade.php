@extends('layouts.app')
@section('content')
    @if ( Auth::user()->facebookLogin )
        @include('includes.facebook.get-campaigns-form')
        <div class="content_form"></div>
        @include('includes.facebook.get-campaigns-info-form')
        <div class="content_form_info"></div>
    @else
        Не установлен customer_id
    @endif
@stop
