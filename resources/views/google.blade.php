@extends('layouts.app')
@section('content')
    @if ( Auth::user()->googleLogin )
        @include('includes.google.get-campaigns-form')
        @include('includes.google.download-report-form')
        <div class="content_form"></div>
    @else
        Не установлен customer_id
    @endif
@stop
