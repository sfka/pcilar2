<form action="{{ url('calltrack/count-calls') }}" method="POST" class="ajax__form">
    {{ csrf_field() }}
    <div class="row">
        <div class="col s12">
            <p>Получить список звонков за выбранный период.</p>
        </div>
    </div>
    <input type="hidden" name="start" value="0">
    <div class="row">
        <div class="col m6 s12 input-field">
            <input id="dateFrom" class="datepicker" type="text" name="dateFrom">
            <label for="dateFrom">Дата начала: dd / mm / yyyy</label>
        </div>
        <div class="col m6 s12 input-field">
            <input id="dateTo" class="datepicker" type="text" name="dateTo">
            <label for="dateTo">Дата конца: dd / mm / yyyy</label>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <select id="entriesPerPage" name="entriesPerPage">
                <option selected>200</option>
                <option>500</option>
                <option>1000</option>
                <option>2000</option>
            </select>
            <label>Количество строк на странице</label>
        </div>
    </div>
    <div class="row">
        <div class="col m6 s12 input-field">
            <button class="btn btn-large waves-effect waves-light" type="submit">
                Получить все звонки
            </button>
        </div>
    </div>
</form>
<div class="content_form"></div>
