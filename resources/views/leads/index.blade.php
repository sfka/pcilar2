@extends('layouts.app')
@section('content')
    <form action="{{ url('leads/') }}" method="POST" class="ajax__form" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col s12">
                Список лидов
            </div>
        </div>
        @if(isset($arResult['USERS']))
            <div class="row">
                <div class="col m6 s12 input-field">
                    <select name="user_name">
                        @foreach($arResult['USERS'] as $user)
                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                    </select>
                    <label>Идентификатор клиента</label>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col m6 s12 input-field">
                <input id="dateFrom" class="datepicker" type="text" name="dateFrom">
                <label for="dateFrom">Дата начала: dd / mm / yyyy</label>
            </div>
            <div class="col m6 s12 input-field">
                <input id="dateTo" class="datepicker" type="text" name="dateTo">
                <label for="dateTo">Дата конца: dd / mm / yyyy</label>
            </div>
        </div>
        <div class="row">
            <div class="col m6 s12 input-field">
                <button class="btn waves-effect waves-light" type="submit">
                    Получить список
                </button>
            </div>
        </div>
    </form>
    <div class="content_form">
        @if(isset($arResult['LEADS']))
            @if(isset($arResult['Date']))
                <h4>Лиды за период: {{ date('d.m.Y', strtotime($arResult['Date']['DateFrom'])) }}
                    - {{ date('d.m.Y', strtotime($arResult['Date']['DateTo'])) }}</h4>
            @endif
            {{--            <a href="/tmp_files/{{ $arResult['LEADS']['FILE']  }}" class="btn waves-effect waves-light">Выгрузить лиды в CSV</a>--}}
            <div class="table_wrap">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Телефон</th>
                        <th scope="col">Дата</th>
                        @if(isset($arResult['USERS']))
                            <th scope="col">Источник</th>
                            <th scope="col">Канал</th>
                            <th scope="col">Ключевое слово</th>
                            <th scope="col">Страница</th>
                            <th scope="col">Файл</th>
                        @endif
                        <th><span class="tooltipped" data-position="bottom"
                                  data-tooltip="Посетитель заинтересован в покупке товара / услуги.">Валидный</span>
                        </th>
                        <th>Комментарий</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?$leadIDs = []; $index = 1; ?>
                    @foreach($arResult['LEADS'] as $lead)
                        <?$leadIDs[] = $lead->id ?>
                        <tr>
                            <td>{{ $index }}</td>
                            <? $index++; ?>
                            <td><span class="nowrap">@if(isset($lead->phone)) {{ $lead->phone }} @endif</span></td>
                            <td>{{ date('d.m.Y H:i:s', strtotime($lead->created_at)) }}</td>
                            @if(isset($arResult['USERS']))
                                <td>@if(isset($lead->utm_source)) {{ $lead->utm_source }} @endif</td>
                                <td>@if(isset($lead->utm_medium)) {{ $lead->utm_medium }} @endif</td>
                                <td>@if(isset($lead->utm_term)) {{ $lead->utm_term }} @endif</td>
                                <td>@if(isset($lead->lead_page)) {{ $lead->lead_page }} @endif</td>
                                <td>@if(isset($lead->file)) {{ $lead->file }} @endif</td>
                            @endif
                            <td class="tac">
                                <label class="valid_label">
                                    <input name="valid" type="checkbox" data-id="{{ $lead->id }}" value="Y"
                                           @if(isset($lead->valid) && $lead->valid == 'Y') checked="checked" @endif
                                           @if(isset($arResult['USERS'])) disabled @endif
                                           class="filled-in ajax__lead_update">
                                    <span><code>Да</code></span>
                                </label>
                                <label class="valid_label">
                                    <input name="valid" type="checkbox" data-id="{{ $lead->id }}" value="N"
                                           @if(isset($lead->valid) && $lead->valid == 'N') checked="checked" @endif
                                           @if(isset($arResult['USERS'])) disabled @endif
                                           class="filled-in ajax__lead_update">
                                    <span><code>Нет</code></span>
                                </label>
                            </td>
                            <td>
                                <textarea class="ajax__lead_update" data-id="{{ $lead->id }}" name="comment" id=""
                                          cols="30" rows="10"
                                          @if(isset($arResult['USERS'])) disabled @endif>{{ $lead->comment }}</textarea>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
@endsection
