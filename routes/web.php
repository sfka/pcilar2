<?php

use Illuminate\Http\Response;

Auth::routes();

Route::get('/all', function(){
   return view('all');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::match(
    ['get', 'post'],
    '/',
    'Dashboard@index'
)->name('dashboard');

Route::get('/calltrack', 'CallTracking@index')->name('calltrack');

Route::get('/yandex', 'Yandex@index')->name('yandex');

Route::get('/facebook', 'Facebook@index')->name('facebook');

Route::get('/google', 'Google@index')->name('google');

Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function () {
    Route::resource('/users', 'UsersController', ['except' => ['show']]);
});
Route::get('/properties', 'Properties@index')->name('admin.properties.index');

Route::get('/compare', 'Dashboard@compareIndex')->name('compare');

Route::match(
    ['get', 'post'],
    'compare/list',
    'Dashboard@getCompare'
);

Route::match(
    ['get', 'post'],
    'google/get-campaigns',
    'AdWordsApiController@getCampaignsAction'
);
Route::match(
    ['get', 'post'],
    'google/download-report',
    'AdWordsApiController@downloadReportAction'
);

Route::match(
    ['get', 'post'],
    'yandex/get-campaigns',
    'Yandex@getCampaignsList'
);

Route::match(
    ['get', 'post'],
    'yandex/report-results',
    'Yandex@getReport'
);

Route::match(
    ['get', 'post'],
    'calltrack/count-calls',
    'CallTracking@getCountCalls'
);

Route::match(
    ['get', 'post'],
    'facebook/get-campaigns',
    'Facebook@getCampaignsList'
);
Route::match(
    ['get', 'post'],
    'facebook/campaign-info',
    'Facebook@getCampaignInfo'
);

Route::match(
  ['get', 'post'],
  'properties/update',
  'Properties@updatePropertiesList'
);

Route::match(
    ['get', 'post'],
    'properties/desc',
    'Properties@getPropertyDescription'
);

Route::match(
    ['get', 'post'],
    'yandex/info',
    'Yandex@getClientInfoPage'
);

Route::match(
    ['get', 'post'],
    'facebook/info',
    'Facebook@getClientInfoPage'
);

Route::match(
    ['get', 'post'],
    'calltrack/info',
    'CallTracking@getClientInfoPage'
);

Route::match(
    ['get', 'post'],
    'google/info',
    'AdWordsApiController@getClientInfoPage'
);

Route::match(
    ['get', 'post'],
    'leads',
    'LeadsUser@index'
)->name('leads');

/*Route::match(
    ['get', 'post'],
    'leads/load',
    'Leads@loadLeads'
);*/

Route::match(
    ['get', 'post'],
    'get_leads',
    'LeadsUser@getLeadsFromWebhook'
);

Route::match(
    ['get', 'post'],
    'leads/update',
    'Venyoo@updateLead'
);

Route::match(
    ['get', 'post'],
    'leads/download',
    'Leads@downloadLeads'
);

Route::match(
    ['get', 'post'],
    'get_yandex',
    'DailyInfoByClient@getYandexInfo'
);

Route::match(
    ['get', 'post'],
    'get_yandex/{user_id}',
    'DailyInfoByClient@getYandexInfo'
);

Route::match(
    ['get', 'post'],
    'get_facebook',
    'DailyInfoByClient@getFacebookInfo'
);

Route::match(
    ['get', 'post'],
    'get_facebook/{user_id}',
    'DailyInfoByClient@getFacebookInfo'
);

Route::match(
    ['get', 'post'],
    'get_google',
    'DailyInfoByClient@getGoogleInfo'
);

Route::match(
    ['get', 'post'],
    'get_google/{user_id}',
    'DailyInfoByClient@getGoogleInfo'
);

Route::match(
    ['get', 'post'],
    'get_calltrack',
    'DailyInfoByClient@getCallTrackInfo'
);

Route::match(
    ['get', 'post'],
    'get_calltrack/{user_id}',
    'DailyInfoByClient@getCallTrackInfo'
);
